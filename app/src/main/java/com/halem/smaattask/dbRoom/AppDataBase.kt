package com.halem.smaattask.dbRoom

import androidx.room.Database
import androidx.room.RoomDatabase
import com.halem.smaattask.modelData.ArticlesItem

@Database(entities = [ArticlesItem::class], version = 3, exportSchema = false)
abstract class AppDataBase : RoomDatabase() {
    abstract val userDao: DaoDataBase
}