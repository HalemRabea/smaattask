package com.halem.smaattask.dbRoom

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.halem.smaattask.modelData.ArticlesItem

@Dao
interface DaoDataBase {

    @Query("SELECT * FROM articles")
    fun findAll(): LiveData<List<ArticlesItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(articles: List<ArticlesItem>)
}