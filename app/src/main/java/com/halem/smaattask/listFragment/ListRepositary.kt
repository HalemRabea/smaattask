package com.halem.smaattask.listFragment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.halem.smaattask.api.Apis
import com.halem.smaattask.dbRoom.DaoDataBase
import com.halem.smaattask.modelData.ArticlesItem
import kotlinx.coroutines.*

class ListRepositary (private val articleApi: Apis, private val userDao: DaoDataBase) {

    var data = userDao.findAll()
    suspend fun refresh() {
        withContext(Dispatchers.IO) {
            val response = articleApi.getAllAsync().await()
            Log.v("status",response.status)
            Log.v("listSize","the size is ${response.articles.size}")
            userDao.add(response.articles)
        }


    }
}