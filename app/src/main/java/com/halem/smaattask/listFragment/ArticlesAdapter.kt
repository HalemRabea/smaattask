package com.halem.smaattask.listFragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.halem.smaattask.databinding.ArticleLayoutBinding
import com.halem.smaattask.modelData.ArticlesItem

class ArticlesAdapter (val onClickListener: OnClickListener) : ListAdapter<ArticlesItem, ArticlesAdapter.StoreViewHolder>(
    DiffCallback
) {
    companion object DiffCallback : DiffUtil.ItemCallback<ArticlesItem>() {
        override fun areItemsTheSame(oldItem: ArticlesItem, newItem: ArticlesItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: ArticlesItem, newItem: ArticlesItem): Boolean {
            return oldItem.source.name == newItem.source.name
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        return StoreViewHolder(
            ArticleLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(item)
        }
    }


    class StoreViewHolder(private var binding: ArticleLayoutBinding):
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ArticlesItem) {
            binding.item = item
            binding.executePendingBindings()
        }
    }


    class OnClickListener(val clickListener: (item: ArticlesItem) -> Unit) {
        fun onClick(item:ArticlesItem) = clickListener(item)
    }
}