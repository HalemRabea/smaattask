package com.halem.smaattask.listFragment

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.halem.smaattask.databinding.ListFragmentBinding
import com.halem.smaattask.utils.LoadingState
import org.koin.android.ext.android.bind

class ListFragment : Fragment() {
    lateinit var binding:ListFragmentBinding
    private val viewModel by viewModel<ListViewModel>()



    companion object {
        fun newInstance() = ListFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= ListFragmentBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel=viewModel

        binding.dataStores.adapter =
            ArticlesAdapter(ArticlesAdapter.OnClickListener {
                viewModel.displaySectionsDetails(it)
            })

        viewModel.loadingState.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.FAILED -> Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
                LoadingState.Status.RUNNING -> Toast.makeText(activity, "Loading", Toast.LENGTH_SHORT).show()
                LoadingState.Status.SUCCESS -> Toast.makeText(activity, "Success", Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.navigateToSelectedArticle.observe(this, Observer {
            if ( it != null ) {
                this.findNavController().navigate(ListFragmentDirections.actionListFragmentToDescriptionFragment(it,it.source.name!!))
                viewModel.displayDetailsComplete()
            }
        })

        return binding.root
    }

}
