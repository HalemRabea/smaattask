package com.halem.smaattask.listFragment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.halem.smaattask.modelData.ArticlesItem
import com.halem.smaattask.utils.LoadingState
import kotlinx.coroutines.launch

class ListViewModel (private val userRepository: ListRepositary) : ViewModel() {

    private val _loadingState = MutableLiveData<LoadingState>()
    val loadingState: LiveData<LoadingState>
        get() = _loadingState

    val data = userRepository.data

    private val _navigateToSelectedArticle = MutableLiveData<ArticlesItem>()

    val navigateToSelectedArticle: LiveData<ArticlesItem>
        get() = _navigateToSelectedArticle

    init {
        fetchData()
    }

     fun fetchData() {
        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING
                userRepository.refresh()
                _loadingState.value = LoadingState.LOADED
            } catch (e: Exception) {
                _loadingState.value = LoadingState.error(e.message)
                Log.v("Error net",e.toString())
            }
        }
    }

    fun displaySectionsDetails(section: ArticlesItem) {
        _navigateToSelectedArticle.value = section
    }

    fun displayDetailsComplete() {
        _navigateToSelectedArticle.value = null
    }
}