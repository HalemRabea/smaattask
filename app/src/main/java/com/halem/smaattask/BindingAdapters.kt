package com.halem.smaattask

import android.net.Uri
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.halem.smaattask.listFragment.ArticlesAdapter
import com.halem.smaattask.modelData.ArticlesItem

@BindingAdapter("formatDate")
fun bindDateWithFormat(tv: TextView, date: String) {
    val newDate: String = date.substring(0, 10)
    tv.text=newDate
}

@BindingAdapter("arrayData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<ArticlesItem>?) {
    val adapter = recyclerView.adapter as ArticlesAdapter
    adapter.submitList(data)
}

@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        //        val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
        val imgUri = Uri.parse(imgUrl).buildUpon().scheme("https").build()
        Glide.with(imgView.context)
            .load(imgUri)
            .apply(
                RequestOptions()
//                        .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image))
            .into(imgView)
    }
}