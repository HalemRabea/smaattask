package com.halem.smaattask

import android.app.Application
import com.halem.smaattask.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf(viewModelModule, repositoryModule, netModule, apiModule, databaseModule,descriptionModule))
        }
    }


}