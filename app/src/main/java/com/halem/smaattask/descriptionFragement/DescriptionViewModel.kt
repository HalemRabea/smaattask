package com.halem.smaattask.descriptionFragement

import android.app.Application
import android.content.ClipboardManager
import android.content.Context
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.halem.smaattask.modelData.ArticlesItem


class DescriptionViewModel(val application: Application, val article: ArticlesItem) : ViewModel() {

    fun urlClicked(view: View){
        val cm: ClipboardManager =
            application.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        cm.text=(view as TextView).text
        Toast.makeText(application, "Copied to clipboard :${view.text}", Toast.LENGTH_SHORT).show()
    }
}
