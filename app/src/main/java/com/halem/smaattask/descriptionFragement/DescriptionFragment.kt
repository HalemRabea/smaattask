package com.halem.smaattask.descriptionFragement

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.halem.smaattask.R
import com.halem.smaattask.databinding.DescriptionFragmentBinding
import com.halem.smaattask.databinding.ListFragmentBinding
import com.halem.smaattask.listFragment.ListViewModel
import kotlinx.android.synthetic.main.description_fragment.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

//DescriptionFragmentArgs.fromBundle(arguments).selectedArticle
class DescriptionFragment : Fragment() {
    lateinit var binding: DescriptionFragmentBinding
//    private val viewModel by viewModel <DescriptionViewModel>()

    val viewModel: DescriptionViewModel by viewModel{ parametersOf(DescriptionFragmentArgs.fromBundle(arguments!!).selectedArticle)}


    companion object {
        fun newInstance() = DescriptionFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= DescriptionFragmentBinding.inflate(inflater)
        binding.lifecycleOwner=this
        binding.viewModel=viewModel


        return binding.root
    }



}
