package com.halem.smaattask.modelData


import android.os.Parcelable
import androidx.room.*
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "articles")
data class ArticlesItem(@SerializedName("publishedAt")
                        @PrimaryKey var publishedAt: String = " ",
                        @SerializedName("author")
                        var author: String ?,
                        @SerializedName("urlToImage")
                        var urlToImage: String ?,
                        @SerializedName("description")
                        var description: String ?,
                        @SerializedName("source")
                        @Embedded var source: Source,
                        @SerializedName("title")
                        var title: String ?,
                        @SerializedName("url")
                        var url: String ?,
                        @SerializedName("content")
                        var content: String ?):Parcelable{
    constructor (): this("","","","",Source("",""),"","","")
}


data class NewsResponse(@SerializedName("totalResults")
                        val totalResults: Int = 0,
                        @SerializedName("articles")
                        val articles: List<ArticlesItem>,
                        @SerializedName("status")
                        val status: String = "")

@Parcelize
data class Source(@SerializedName("name")
                  val name: String ?,
                  @SerializedName("id")
                  val id: String ?): Parcelable


