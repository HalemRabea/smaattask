package com.halem.smaattask.api

import com.halem.smaattask.modelData.NewsResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface Apis {

    @GET("v2/everything?q=bitcoin&apiKey=031c6f79dcb1478fb808a6cb6075a1c6")
    fun getAllAsync(): Deferred<NewsResponse>
}