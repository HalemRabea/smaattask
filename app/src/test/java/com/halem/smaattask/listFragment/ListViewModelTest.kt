package com.halem.smaattask.listFragment

import android.os.UserManager
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.halem.smaattask.api.Apis
import com.halem.smaattask.dbRoom.DaoDataBase
import com.halem.smaattask.modelData.ArticlesItem
import com.halem.smaattask.modelData.NewsResponse
import com.halem.smaattask.utils.LoadingState
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.notNull
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations


class ListViewModelTest {

    private lateinit var viewModel: ListViewModel
    private lateinit var listRepositary: ListRepositary
    private lateinit var dao: DaoDataBase
    private lateinit var stateObserver: Observer<LoadingState>
//    private lateinit var dataObserver: Observer<List<ArticlesItem>?>
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @ExperimentalCoroutinesApi
    @ObsoleteCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        listRepositary = mock()
//        dao= mock()
        runBlocking {
            whenever(listRepositary.refresh()).thenReturn(Unit)
        }
        viewModel = ListViewModel(listRepositary)
//        dataObserver=mock()
        stateObserver = mock()
    }

    @ObsoleteCoroutinesApi
    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }


    @Test
    fun `when refresh is called, then observer is updated with success state loaded`() = runBlocking {
        viewModel.loadingState.observeForever(stateObserver)
        delay(10)
        verify(listRepositary).refresh()
        verify(stateObserver).onChanged(LoadingState.LOADED)
    }


    @Test
    fun `when refresh is called, then observer of data is updated`() = runBlocking {
        delay(10)
        verify(listRepositary).refresh()
        Assert.assertEquals(viewModel.data, notNull())



    }

}